const path = require('path');

const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');


const extractSass = new ExtractTextPlugin({
    filename: "[name].css"
});


const config = {
    devServer: {
        contentBase: './build',
        historyApiFallback: true,
        port: 9000
    },
    devtool: 'source-map',
    entry: {
        app: './src/index.js',
        vendor: [
            'baobab',
            'baobab-react',
            'react',
            'react-dom'
        ]
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                include: path.resolve(__dirname, 'src'),
                use: 'babel-loader'
            },
            {
                test: /\.scss$/,
                use: extractSass.extract({
                    use: [
                        { loader: "css-loader", options: { sourceMap: true } },
                        { loader: "sass-loader", options: { sourceMap: true, includePaths: [path.resolve(__dirname, 'src/app/styles/')] } }
                    ],
                    fallback: "style-loader"
                })
            }
        ]
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'build')
    },
    plugins: [
        new HtmlWebpackPlugin(),
        new webpack.ProvidePlugin({
            React: 'react',
            ReactDOM: 'react-dom'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: Infinity
        }),
        extractSass
    ],
    profile: true,
    resolve: {
        modules: ['node_modules', './src/'],
        extensions: ['.js', '.json', '.scss']
    },
    watch: true,
    watchOptions: {
        aggregateTimeout: 1000
    }
}

module.exports = config;
