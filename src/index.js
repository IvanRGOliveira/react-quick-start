import App from './app/app';

let appContainer = document.createElement('div');
appContainer.id = 'app-container';
document.body.append(appContainer)

ReactDOM.render(<App />, document.getElementById('app-container'));
