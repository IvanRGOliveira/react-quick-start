//Dependencies
import { root } from 'baobab-react/higher-order'
import Baobab from 'baobab';

//Components
import Game from './components/game';

//Data
import { data } from './services/data';

const Store = new Baobab(data);

@root(Store)
class App extends React.Component {
    render() {
        return (
            <Game />
        )
    }
}

export default App;
