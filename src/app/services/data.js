export const GAME_STATE = Object.freeze({
    INPUT: 'input',
    PLAYING: 'playing',
    END: 'end'
});

export const data = {
    game: {
        state: GAME_STATE.INPUT
    },
    grid: {
        Height: 1,
        Width: 1
    },
    player: {
        X: 0,
        Y: 0
    },
    dirt: [],
    errors: [],
    stats: {
        dirt: []
    }
}
