export function cleanDirtPatch(tree, data) {
    tree.select('dirt').set(data);
}

export function addToStats(tree, data) {
    tree.push(['stats', 'dirt'], data);
}
