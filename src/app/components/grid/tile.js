export default function Tile({ location: { X, Y }, hasDirtPatch, hasHoover, isDebugging }) {
    return (
        <div className={`tile ${ hasDirtPatch ? 'dirt' : '' }`}>
            { hasHoover ? <div className="hoover" /> : null }
            { isDebugging ? <div className="debug">{ `X: ${ X } Y: ${ Y }` }</div> : null}
        </div>
    )
}
