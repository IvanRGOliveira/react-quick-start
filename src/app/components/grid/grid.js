//Dependencies
import { branch } from 'baobab-react/higher-order';

//Actions
import * as actions from './actions/grid-actions';

//Components
import Tile from './tile';

//Styles
require('./styles/grid.scss');


@branch({
    dirt: ['dirt'],
    grid: ['grid'],
    player: ['player']
})
class Grid extends React.Component {

    buildGrid() {
        const { dirt, grid: { Height, Width }, player } = this.props;
        let grid = [];

        this.cleanDirt(dirt, player);

        let i = Height - 1;
        for(; i >= 0; i--) {
            let row = [];
            for(let j = 0; j < Width; j++) {
                row.push(
                    <Tile key={`${ j } - ${ i }`}
                        location={{ X: j, Y: i }}
                        hasDirtPatch={ this.getHasDirtPatch(dirt, j, i) }
                        hasHoover={ this.getHasHoover(player, j, i) }
                        isDebugging={ true }
                    />
                );
            }
            grid.push(<div key={ i } className="row">{ row }</div>);
        }

        return (
            <div className="grid">
                { grid }
            </div>
        );
    }

    cleanDirt(dirt, player) {
        const isOverDirtPatch = dirt.find(dirtPatch => {
            return dirtPatch.X == player.X && dirtPatch.Y == player.Y && dirtPatch.cleaned == false
        });

        if(isOverDirtPatch) {
            let tempDirt = dirt.filter(dirtPatch => {
                return dirtPatch != isOverDirtPatch
            });
            let cleanedDirtPatch = Object.assign({}, isOverDirtPatch, { cleaned: true });
            tempDirt.push(cleanedDirtPatch);

            this.props.dispatch(
                actions.addToStats,
                cleanedDirtPatch
            );

            this.props.dispatch(
                actions.cleanDirtPatch,
                tempDirt
            );
        }
    }

    getHasDirtPatch(dirt, x, y) {
        return dirt.filter(dirtPatch => {
            return dirtPatch.X == x && dirtPatch.Y == y && dirtPatch.cleaned == false
        }).length > 0;
    }

    getHasHoover(player, x, y) {
        return player.X == x && player.Y == y;
    }

    render() {
        return this.buildGrid();
    }
}

export default Grid;
