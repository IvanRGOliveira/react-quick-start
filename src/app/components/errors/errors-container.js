//Dependencies
import { branch } from 'baobab-react/higher-order';

//Actions
import * as errors from '../../services/error-actions';

@branch({
    errors: ['errors']
})
class ErrorsContainer extends React.Component {

    render() {
        return (
            <div className="errors-container">

            </div>
        )
    }
}

export default ErrorsContainer;
