import { branch } from 'baobab-react/higher-order';

@branch({
    dirt: ['dirt'],
    stats: ['stats', 'dirt']
})
class Stats extends React.Component {
    render() {
        const { dirt, stats } = this.props;
        return (
            <div className="stats">
                <h3>Stats:</h3>
                { `You cleaned ${ stats.length } out of ${ dirt.length } dirt patches!` }
                { dirt.length == stats.length ? <h4>Congratulations!</h4> : null }
            </div>
        )
    }
}

export default Stats;
