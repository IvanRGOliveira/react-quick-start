//Dependencies
import { branch } from 'baobab-react/higher-order';

//Components
import Grid from './grid/grid';
import UserInput from './user-input/user-input';
import PlayerMover from './user-input/player-mover';
import Stats from './stats/stats';

//Data
import { GAME_STATE } from '../services/data';


@branch({
    game: ['game']
})
class Game extends React.Component {
    render() {
        const { game } = this.props;

        return (
            <div className="game">
                <h1>The Game</h1>
                <h2>It's time to play the game!</h2>
                {
                    game.state === GAME_STATE.INPUT
                        ? <UserInput />
                        : [<Stats key="stats" />, <Grid key="grid" />, <PlayerMover key="player=mover" />]
                }
            </div>
        )
    }
}

export default Game;
