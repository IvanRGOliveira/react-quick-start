//Dependencies
import { branch } from 'baobab-react/higher-order';

//Actions
import * as actions from './actions/user-input-actions';
import * as errors from '../../services/error-actions';

//Components
import Input from './input';
import DirtPatchesList from './dirt-patches-list';

@branch({
    dirt: ['dirt']
})
class DirtPatchesInput extends React.Component {
    constructor() {
        super();

        this.state = {
            X: '',
            Y: ''
        }

        this.addPatch = this.addPatch.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
    }

    addPatch() {
        const { X, Y } = this.state;
        if(!this.validateInput()) {
            this.props.dispatch(
                errors.addError,
                { message: 'Dirt Patches: Please insert the coordinates on both fields.' }
            )
        } else {
            this.props.dispatch(
                actions.addPatch,
                { X: Number(X), Y: Number(Y), cleaned: false }
            );
            this.resetInput();
        }
    }

    changeHandler(e, label) {
        const value = e.target.value;
        this.setState({
            [label]: Number(value)
        });
    }

    resetInput() {
        this.setState({
            X: '',
            Y: ''
        });
    }

    validateInput() {
        const { dirt } = this.props;
        const { X, Y } = this.state;
        if(!X || !Y) {
            return false;
        } else {
            const patchExists = dirt.filter((patch) => { return patch.X == X && patch.Y == Y }).length > 0;
            if(patchExists) return false;
        }

        return true;
    }

    render() {
        const { X, Y } = this.state;
        return (
            <div className="dirt-patches">
                <h3>Dirt Patches</h3>
                <Input label="X" changeHandler={ this.changeHandler } type="number" value={ X } />
                <Input label="Y" changeHandler={ this.changeHandler } type="number" value={ Y } />
                <button onClick={ this.addPatch }>Add</button>
            </div>
        )
    }
}

export default DirtPatchesInput;
