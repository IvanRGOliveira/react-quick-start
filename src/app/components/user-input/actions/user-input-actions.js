export function addPatch( tree, data ) {
    tree.push('dirt', data);
}

export function start( tree, data ) {
    tree.select([ 'game', 'state' ]).set(data);
}

export function updateGrid( tree, data ) {
    tree.select([ 'grid', data.label ]).set(data.value);
    tree.commit();
}

export function updatePlayer( tree, data ) {
    tree.select([ 'player', data.label ]).set(data.value);
    tree.commit();
}
