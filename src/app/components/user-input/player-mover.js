//Dependencies
import { branch } from 'baobab-react/higher-order';

//Actions
import * as actions from './actions/player-mover-actions';

//Styles
require('./styles/player-mover.scss');

const PLAYER_MOVEMENT = Object.freeze({
    NORTH: 'north',
    SOUTH: 'south',
    EAST: 'east',
    WEST: 'west'
})

@branch({
    grid: ['grid'],
    player: ['player']
})
class PlayerMover extends React.Component {

    getMoveValue(movement) {
        const { player } = this.props;
        if(movement.hasOwnProperty('Y')) {
            return { Y: player.Y + movement.Y };
        }

        if(movement.hasOwnProperty('X')) {
            return { X: player.X + movement.X };
        }

    }

    isMoveValid(movement, axis) {
        const { player, grid } = this.props;
        const move = player[axis] + movement[axis];
        if(move < 0 || move >= (axis == 'Y' ? grid.Height : grid.Width)) {
            return false
        } else {
            return true;
        }
    }

    moverHandler(e, direction) {
        const { player } = this.props;
        const movement = this.normaliseMovement(direction);
        if(this.validateMove(movement)) {
            this.props.dispatch(
                actions.movePlayer,
                Object.assign({}, player, this.getMoveValue(movement))
            )
        } else {
            return;
        }
    }

    normaliseMovement(direction) {
        switch(direction) {
            case PLAYER_MOVEMENT.NORTH:
                return { Y: 1 }
            case PLAYER_MOVEMENT.SOUTH:
                return { Y: -1 }
            case PLAYER_MOVEMENT.EAST:
                return { X: 1 }
            case PLAYER_MOVEMENT.WEST:
                return { X: -1 }
            default:
                return { X: 0 }
        }
    }

    validateMove(movement) {
        const { grid } = this.props;

        if(movement.hasOwnProperty('Y')) {
           return this.isMoveValid(movement, 'Y');
        }

        if(movement.hasOwnProperty('X')) {
            return this.isMoveValid(movement, 'X');
        }

        return false;
    }

    render() {
        return (
            <div className="player-mover">
                <button className="mover north" onClick={ (e) => this.moverHandler(e, PLAYER_MOVEMENT.NORTH) }>North</button>
                <button className="mover west" onClick={ (e) => this.moverHandler(e, PLAYER_MOVEMENT.WEST) }>West</button>
                <button className="mover east" onClick={ (e) => this.moverHandler(e, PLAYER_MOVEMENT.EAST) }>East</button>
                <button className="mover south" onClick={ (e) => this.moverHandler(e, PLAYER_MOVEMENT.SOUTH) }>South</button>
            </div>
        )
    }

}

export default PlayerMover;
