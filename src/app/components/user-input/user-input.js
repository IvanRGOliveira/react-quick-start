//Dependencies
import { branch } from 'baobab-react/higher-order';

//Actions
import * as actions from './actions/user-input-actions';
import * as errors from '../../services/error-actions';

//Components
import DirtPatchesInput from './dirt-patches-input';
import DirtPatchesList from './dirt-patches-list';
import Input from './input';

//Data
import { GAME_STATE } from '../../services/data';

@branch({
    dirt: [ 'dirt' ],
    grid: [ 'grid' ],
    player: [ 'player' ]
})
class UserInput extends React.Component {

    constructor() {
        super();

        this.gridHandler = this.gridHandler.bind(this);
        this.playerHandler = this.playerHandler.bind(this);
        this.start = this.start.bind(this);
        this.validateGame = this.validateGame.bind(this);
    }

    gridHandler(e, label) {
        this.props.dispatch(
            actions.updateGrid,
            { label: label, value: e.target.value }
        );
    }

    playerHandler(e, label) {
        this.props.dispatch(
            actions.updatePlayer,
            { label: label, value: Number(e.target.value) }
        );
    }

    start() {
        if(this.validateGame()) {
            this.props.dispatch(
                actions.start,
                GAME_STATE.PLAYING
            );
        }
    }

    validateGame() {
        const { dirt, grid, player } = this.props;
        if(grid.Height == 0 && grid.Width == 0) {
            this.props.dispatch(
                errors.addError,
                { message: 'Make sure the grid ' }
            )
            return false;
        } else {
            return true;
        }
    }

    render() {
        const { game, grid, player } = this.props;
        return (
            <div className="user-input">
                <div className="group">
                    <h3>Grid Dimensions</h3>
                    <Input changeHandler={ this.gridHandler } label="Height" type="number" value={ grid.Height } />
                    <Input changeHandler={ this.gridHandler } label="Width" type="number" value={ grid.Width } />
                </div>
                <div className="group">
                    <h3>Initial Hoover Position</h3>
                    <Input changeHandler={ this.playerHandler } label="X" type="number" value={ player.X } />
                    <Input changeHandler={ this.playerHandler } label="Y" type="number" value={ player.Y } />
                </div>
                <div className="group">
                    <DirtPatchesInput />
                </div>
                <div className="group">
                    <DirtPatchesList />
                </div>
                <button onClick={ this.start }>Start</button>
            </div>
        )
    }
}

export default UserInput;
