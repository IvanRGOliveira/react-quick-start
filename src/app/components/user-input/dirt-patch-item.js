export default function DirtPatchItem({ X, Y }) {
    return (
        <li>X: { X } - Y: { Y }</li>
    )
}
