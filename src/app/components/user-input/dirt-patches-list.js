//Dependencies
import { branch } from 'baobab-react/higher-order';

//Actions
import * as actions from './actions/user-input-actions';

//Components
import DirtPatchItem from './dirt-patch-item';
import Input from './input';


@branch({
    dirt: ['dirt']
})
class DirtPatchesList extends React.Component {
    constructor() {
        super();

        this.listPatches = this.listPatches.bind(this);
    }

    listPatches() {
        const { dirt } = this.props;
        if( !dirt.length ) {
            return <li>No dirt patches.</li>
        }

        return dirt.map(patch => {
            return (
                <DirtPatchItem key={`${patch.X}-${patch.Y}`} { ...patch } />
            )
        })
    }

    render() {
        return (
            <ul className="dirt-patches-list">
                { this.listPatches() }
            </ul>
        )
    }
}

export default DirtPatchesList;
