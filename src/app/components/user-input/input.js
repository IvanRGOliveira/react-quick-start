export default function Input({ changeHandler, label, type, value }) {
    return (
        <label>{ label }<input type={ type } onChange={ (e) => changeHandler(e, label) } value={ value } /></label>
    );
}
