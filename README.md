# Instructions to run:
- Have Node v6+ installed
- Navigate to the project's root folder
- Run `npm install` to install all the dependencies
- Run `npm start`, it should open your browser window automatically


# Note:

This is far from a finished "product" but I'm aware of it.

I'd like to thank you for the opportunity and patience over the last 2 weeks.
